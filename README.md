# sisop-praktikum-modul-2-2023-BS-D08
> Kelompok D08

***

## Anggota Kelompok
1. I Gusti Ngurah Ervan Juli Ardana (5025211205)
2. Ghifari Maaliki Syafa Syuhada (5025211158)
3. Elmira Farah Azalia (5025211197)

---
### Soal1
#### - Pertanyaan:
Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.

a. Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

kaggle datasets download -d bryanb/fifa-player-stats-database

Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

b. Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

c. Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

d. Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

e. Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.

#### - Solusi: 
a&b.  pada point a dan b kita diminta untuk membuat sebuah file bernama storage.c kemudian melakukan download pada dataset denga sintaks berikut:
```
system("kaggle datasets download -d bryanb/fifa-player-stats-database");
``` 
pastikan sebelum mendownload file tersebut sudah membuat token baru pada akun kaggle masing masing sehingga mendapatkan file kaggle.json pada direktori kerja masing masing. sehingga setelah mendapatkan file kaggle.json dan letaknya sama dengan direktori kita saat ini, maka kita dapat mendownload dataset tersebut. setelah mendownload kita melakukan unzip dari hasil download tersebut:
```
system("unzip fifa-player-stats-database.zip -d .");
```
setelah berhasil di unzip kita diminta untuk menampilkan data pemain yang memenuhi kondisi pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. untuk menampilkan pemain tersebut kita dapat menggunakan awk untuk menyelesaikanya. 

```
system("sort -t',' -k8nr FIFA23_official_data.csv | awk -F',' 'BEGIN { rank = 1; printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", \"No\", \"Name\", \"Age\", \"Club\", \"Nationality\", \"Potential\", \"Photo\" } FNR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", rank, $2, $3, $9, $5, $8, $4; rank++ }'");
```
perintah diatas dimulai dengan melakukan sort berdasarkan number pada kolom ke-8 (Potential) pada data FIFA23_official_data yang telah kita download. setelah kita sort kita membuat header agar lebih rapi menggunakan BEGIN dan menginisialisasi rank dimulai dari 1. setelah header jadi kemudian kita membuat filter pemain dengan FNR > 1 agar system memulai pengecekan dari baris 2(karena baris 1 header), setelah itu kolom ke-3 (age) kurang dari 25, kolom ke-8(potential) lebih dari 85 dan kolom ke-9 (club) tidak Manchester City. Setelah di filter kemudian kita hanya perlu menampilkan semua data yang telah lolos dari filter tersebut dan menambahkan rank sebanyak 1 setiap menambahkan data. berikut merupakan kode a&b :
```
#include <stdio.h>
#include <stdlib.h>

int main() {
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");
    system("unzip fifa-player-stats-database.zip -d .");
    printf("\nBerikut dibawah ini merupakan data pemain muda yang memiliki potential diatas 85: \n\n ");
    system("sort -t',' -k8nr FIFA23_official_data.csv | awk -F',' 'BEGIN { rank = 1; printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", \"No\", \"Name\", \"Age\", \"Club\", \"Nationality\", \"Potential\", \"Photo\" } FNR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", rank, $2, $3, $9, $5, $8, $4; rank++ }'");
    return 0;
}

```
berikut merupakan output ketika saya menjalankan program storage.c
![Screenshot_from_2023-06-02_13-49-48](/uploads/78ffffb5aaa6d97b2b6431dcc5b268b0/Screenshot_from_2023-06-02_13-49-48.png)

c. pada pint c kita diminta untuk membuat sebuah dockerfile dengan perintah yang sama dengan storgae.c kemudian menghasilkan docker image dari dockerfile tersebut dan pada akhirnya menciptakan docker container.Pertama tama kita harus membuat dockerfile terlebih dahulu pada direktori yang sama dengan storage.c. setelah membuat Dockerfile kita harus mendeklarasikan sistem operasi yang kita gunakan dan mengupdate :
```
FROM ubuntu:latest
# update package ubuntu
RUN apt-get update -y
```
version yang saya gunakan diatas yaitu latest karena penyimpanan yang tidak terlalu besar dan sudah sesuai dengan apa yang diminta program. setelah itu kita harus menginstall python dan selanjutnya menginstall kaggle:
```
#intall python
RUN apt-get install -y unzip python3-pip
# Install kaggle
RUN pip3 install kaggle
```
setelah berhasil, kemudian kita harus copy kaggle.json dan  ke container kita dan mengubah izin aksesnya agar nantinya kita dapat menginstall dataset yang diminta. setelah kita berhasil mendownload dataset kemudian kita harus melakukan unzip pada hasil download kita:
```
# memindahkan kaggle api ke container
COPY kaggle.json /root/.kaggle/
RUN chmod 600 /root/.kaggle/kaggle.json
# Download dataset 
RUN kaggle datasets download -d bryanb/fifa-player-stats-database
# Extract the dataset
RUN unzip fifa-player-stats-database.zip -d /

```
setelah kita berhasil melakukan unzip, kemudian kita harus menginstall awk terlebih dahulu agar kita bisa menjalankan storage.c dan menyalin storage.c yang terdapat pada direktorikita ke container 

```
RUN apt-get install -y gawk
# Copy storage.c
COPY storage.c /
# Build the executable program
RUN gcc -o storage storage.c
```
terakhir kita memerlukan sebuah perintah yang harus dijalankan ketika kita ingin create sebuah container. dan ketika kita menjalankanya kita akan menghasilkan output yang sama seperti program storage.c 

```
# Set the command to run the program
CMD ["./storage"]
```
Berikut merupakan full kode dari perintah pada point c:
```
FROM ubuntu:latest
# update package ubuntu
RUN apt-get update -y
#intall python
RUN apt-get install -y unzip python3-pip
# Install kaggle
RUN pip3 install kaggle
# memindahkan kaggle api ke container
COPY kaggle.json /root/.kaggle/
RUN chmod 600 /root/.kaggle/kaggle.json
# Download dataset 
RUN kaggle datasets download -d bryanb/fifa-player-stats-database
# Extract the dataset
RUN unzip fifa-player-stats-database.zip -d /
# Install awk
RUN apt-get install -y gawk
# Copy storage.c
COPY storage.c /
# Build the executable program
RUN gcc -o storage storage.c
# Set the command to run the program
CMD ["./storage"]
```
setelah Dockerfile kita jadi kita harus build terlebih dahulu menjadi docker image dengan perintah 
```
sudo docker build -t ngurahervan/praktikum4 praktikum4
```
hasilnya akan seperti dibawah:
![Screenshot_from_2023-06-02_13-56-15](/uploads/73c3f10fbfca578beb09405e97350508/Screenshot_from_2023-06-02_13-56-15.png)

kemudian setelah kita build kita harus membuat sebuah container dan menjalankan container tersebut:

```
#create
sudo docker container create --name pemain ngurahervan/praktikum4

#start
sudo docker start player
```
hasilnya akan sebagai berikut gambar:
![Screenshot_from_2023-06-02_13-58-19](/uploads/dd9e31b1531496581460145a88e5daad/Screenshot_from_2023-06-02_13-58-19.png)

dan setelah berhasil maka kita harus melihat hasil dari container kita dengan perintah:
```
docker container logs command
```
Hasilnya akan sebagai berikut :
![Screenshot_from_2023-06-02_13-59-15](/uploads/793223f0659655cf04cca68e4c95ec61/Screenshot_from_2023-06-02_13-59-15.png)

d. pada point d kita hanya diminta untuk melakukan pull ke docker hub. caranya kita hanya perlu melakukan login dan hanya melakukan pull dengan memilih docker image mana yang akan kita pull pada terminal kita

e. pada point e kita diminta untuk membuat folder barcelona dan napoli kemudian membuat docker compose yang dijalankan pada masing masing folder tersebut dan melakukan instance sebanyak 5. Pertama tama kita perlu membuat sebuah file docker yang bernama docker-compose.yml berisi berikut
```
version: '3'
services:
  Barcelona:
    build: ./Barcelona
    image: ngurahervan/praktikum4
    deploy:
      replicas: 5

  Napoli:
    build: ./Napoli
    image: ngurahervan/praktikum4
    deploy:
      replicas: 5

```
pada docker compose diatas kita menggunakan Versi 3 dari Docker Compose yang akan kita gunakan dalam konfigurasi. kemudian kita membuat 2 service (komponen utama) yaitu bernama Barcelona dan Napoli. isi dari kedua service tersebut sama. pada service barcelona kita menentukan  direktori dimana Docker akan melakukan build image untuk service barcelona. untuk barcelona melakukan di direktori Barcelona untuk napoli melakukakn di direktori Napoli. kemudian keduanya sama sama menjalankan image ngurahervan/praktikum4 dan melakukan replica sebanyak 5 kali sehingga nanti ketika dijalankan hasilnya sebagai berikut
![Screenshot_from_2023-06-02_14-07-31](/uploads/62f6f9f4c040350036ab6826a7ab7ac3/Screenshot_from_2023-06-02_14-07-31.png)
![Screenshot_from_2023-06-02_14-07-52](/uploads/d7168bc10bc136fdc57ebacb5d8b3c74/Screenshot_from_2023-06-02_14-07-52.png)

---
### Soal4
#### - Pertanyaan:
Pada suatu masa, terdapat sebuah perusahaan bernama Bank Sabar Indonesia yang berada pada masa kejayaannya. Bank tersebut memiliki banyak sekali kegiatan-kegiatan yang  krusial, seperti mengolah data nasabah yang mana berhubungan dengan uang. Suatu ketika, karena susahnya maintenance, petinggi bank ini menyewa seseorang yang mampu mengorganisir file-file yang ada di Bank Sabar Indonesia. 
Salah satu karyawan di bank tersebut merekomendasikan Bagong sebagai seseorang yang mampu menyelesaikan permasalahan tersebut. Bagong memikirkan cara terbaik untuk mengorganisir data-data nasabah dengan cara membagi file-file yang ada dalam bentuk modular, yaitu membagi file yang mulanya berukuran besar menjadi file-file chunk yang berukuran kecil. Hal ini bertujuan agar saat terjadi error, Bagong dapat mudah mendeteksinya. Selain dari itu, agar Bagong mengetahui setiap kegiatan yang ada di filesystem, Bagong membuat sebuah sistem log untuk mempermudah monitoring kegiatan di filesystem yang mana, nantinya setiap kegiatan yang terjadi akan dicatat di sebuah file log dengan ketentuan sebagai berikut.

•	Log akan dibagi menjadi beberapa level, yaitu REPORT dan FLAG.

•	Pada level log FLAG, log akan mencatat setiap kali terjadi system call rmdir (untuk menghapus direktori) dan unlink (untuk menghapus file). Sedangkan untuk sisa operasi lainnya, akan dicatat dengan level log REPORT.

•	Format untuk logging yaitu sebagai berikut.
[LEVEL]::[yy][mm][dd]-[HH]:[MM]:[SS]::[CMD]::[DESC ...]

Contoh:
REPORT::230419-12:29:28::RENAME::/bsi23/bagong.jpg::/bsi23/bagong.jpeg
REPORT::230419-12:29:33::CREATE::/bsi23/bagong.jpg
FLAG::230419-12:29:33::RMDIR::/bsi23

Tidak hanya itu, Bagong juga berpikir tentang beberapa hal yang berkaitan dengan ide modularisasinya sebagai berikut yang ditulis dalam modular.c.

a.	Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 

b.	Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.

c.	Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.

d.	Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

e. Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

#### - Solusi:
Membuat kode modular.c
```
#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <limits.h>

#define LOG_FILE "/home/elmiraazalia/fs_module.log"
#define MODULAR_PREFIX "module_"
#define CHUNK_SIZE 1024

static FILE *log_file;

void log_event(const char *level, const char *cmd, const char *old_path, const char *new_path) {
    time_t now;
    struct tm *timeinfo;
    char timestamp[20];

    time(&now);
    timeinfo = localtime(&now);

    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", timeinfo);

    if (new_path != NULL) {
        fprintf(log_file, "%s::%s::%s::%s\n", level, timestamp, cmd, old_path);
        fprintf(log_file, "%s::%s::%s\n", level, timestamp, new_path);
    } else {
        fprintf(log_file, "%s::%s::%s\n", level, timestamp, old_path);
    }

    fflush(log_file);
}

int is_modular_dir(const char *path) {
    return strncmp(path, MODULAR_PREFIX, strlen(MODULAR_PREFIX)) == 0;
}

int is_modular_file(const char *path) {
    const char *filename = strrchr(path, '/');
    if (filename != NULL)
        return strncmp(filename + 1, MODULAR_PREFIX, strlen(MODULAR_PREFIX)) == 0;
    return 0;
}

void get_modular_chunk_path(const char *path, int chunk_index, char *chunk_path) {
    const char *filename = strrchr(path, '/');
    strncpy(chunk_path, path, filename - path);
    chunk_path[filename - path] = '\0';
    strcat(chunk_path, "/");
    strncat(chunk_path, filename + 1, strlen(filename) - 4);
    sprintf(chunk_path + strlen(chunk_path), ".%03d", chunk_index);
}

int get_chunk_count(off_t size) {
    return (size + CHUNK_SIZE - 1) / CHUNK_SIZE;
}

static int fs_getattr(const char *path, struct stat *stbuf) {
    int res;
    res = lstat(path, stbuf);

    if (res == -1) return -errno;

    stbuf->st_mode |= 0777;

    return 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path);
    if (dp == NULL) {
        return -errno;
    }

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if (filler(buf, de->d_name, &st, 0)) break;
    }

    closedir(dp);
    return 0;
}

static int fs_mkdir(const char *path, mode_t mode) {
    int res;
    res = mkdir(path, mode);
    if (res == -1) {
        return -errno;
    }

    chmod(path, mode);

    log_event("REPORT", "MKDIR", path, NULL);

    return 0;
}

static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
    char fullpath[PATH_MAX];
    snprintf(fullpath, PATH_MAX, "%s%s", (char *)fuse_get_context()->private_data, path);

    int res;
    res = creat(fullpath, mode);
    if (res == -1) {
        return -errno;
    }

    close(res);

    log_event("REPORT", "CREATE", path, NULL);

    return 0;
}

static int fs_rename(const char *oldpath, const char *newpath) {
    int res;
    res = rename(oldpath, newpath);
    if (res == -1) {
        return -errno;
    }

    log_event("REPORT", "RENAME", oldpath, newpath);

    return 0;
}

static int fs_rmdir(const char *path) {
    int res;
    res = rmdir(path);
    if (res == -1) {
        return -errno;
    }

    log_event("FLAG", "RMDIR", path, NULL);

    return 0;
}

static int fs_unlink(const char *path) {
    int res;
    res = unlink(path);
    if (res == -1) {
        return -errno;
    }

    log_event("FLAG", "UNLINK", path, NULL);

    return 0;
}

static struct fuse_operations fs_oper = {
    .getattr = fs_getattr,
    .readdir = fs_readdir,
    .mkdir = fs_mkdir,
    .create = fs_create,
    .rename = fs_rename,
    .rmdir = fs_rmdir,
    .unlink = fs_unlink,
};

int main(int argc, char *argv[]) {
    umask(0);

    log_file = fopen(LOG_FILE, "a");
    if (log_file == NULL) {
        perror("Cannot open log file");
        return 1;
    }

    return fuse_main(argc, argv, &fs_oper, NULL);
}
```
Kode merupakan implementasi sebuah file system module menggunakan FUSE (Filesystem in Userspace). FUSE memungkinkan pengembang untuk membuat sistem file khusus yang berjalan di dalam ruang pengguna (userspace) tanpa perlu mengubah kernel.

Berikut adalah penjelasan singkat mengenai kode:

1. Fungsi `log_event`: Fungsi ini digunakan untuk mencatat peristiwa (event) dalam log file. Fungsi ini menerima parameter level ("REPORT", "FLAG"), cmd ("MKDIR", "RENAME", "CREATE", "RMDIR", "UNLINK"), old_path (path lama), dan new_path (path baru). Fungsi ini menggunakan fungsi `fprintf` untuk menulis informasi ke log file.

2. Fungsi `is_modular_dir`: Fungsi ini digunakan untuk memeriksa apakah suatu path adalah direktori modular. Direktori modular adalah direktori yang nama filenya diawali dengan "module_".

3. Fungsi `is_modular_file`: Fungsi ini digunakan untuk memeriksa apakah suatu path adalah file modular. File modular adalah file yang nama filenya diawali dengan "module_" setelah karakter '/' terakhir dalam path.

4. Fungsi `get_modular_chunk_path`: Fungsi ini digunakan untuk mendapatkan path chunk modular berdasarkan path file modular dan indeks chunk. Path chunk modular adalah path yang memiliki ekstensi ".xxx" di belakangnya, di mana "xxx" adalah indeks chunk dengan format tiga digit.

5. Fungsi `get_chunk_count`: Fungsi ini digunakan untuk menghitung jumlah chunk yang diperlukan berdasarkan ukuran file. Fungsi ini mengembalikan jumlah chunk yang dibutuhkan berdasarkan ukuran file dan ukuran chunk yang ditentukan (CHUNK_SIZE).

Selanjutnya, terdapat implementasi fungsi-fungsi dalam FUSE:

1. `fs_getattr`: Fungsi ini mengembalikan atribut file (struct stat) untuk path yang diberikan. Fungsi ini menggunakan fungsi `lstat` untuk mendapatkan atribut file, dan kemudian mengatur mode file menjadi 0777.

2. `fs_readdir`: Fungsi ini membaca isi dari suatu direktori dan memasukkan informasi file (nama, atribut) ke dalam buffer menggunakan fungsi `filler`. Fungsi ini menggunakan fungsi `readdir` untuk membaca direktori.

3. `fs_mkdir`: Fungsi ini membuat direktori baru dengan mode yang ditentukan. Fungsi ini menggunakan fungsi `mkdir` untuk membuat direktori, dan kemudian menggunakan `chmod` untuk mengatur mode direktori yang dibuat.

4. `fs_create`: Fungsi ini membuat file baru dengan mode yang ditentukan. Fungsi ini menggunakan fungsi `creat` untuk membuat file, dan kemudian menutup file yang dibuat.

5. `fs_rename`: Fungsi ini mengganti nama (rename) dari suatu file atau direktori. Fungsi ini menggunakan fungsi `rename` untuk mengganti nama file atau direktori.

6. `fs_rmdir`: Fungsi ini menghapus direktori kosong. Fungsi ini menggunakan fungsi `rmdir` untuk menghapus direktori.

7. `fs_unlink`: Fungsi ini menghapus (unlink) suatu file. Fungsi ini menggunakan fungsi `unlink` untuk menghapus file.

9. Struktur `fuse_operations`:
   - Struktur ini mendefinisikan operasi-operasi sistem berkas yang akan diimplementasikan.
   - Masing-masing operasi memiliki tautan ke fungsi yang sesuai.

10. Fungsi `main`:
    - Mengatur mode izin file (`umask(0)`).
    - Membuka file log dengan mode "a" (menambahkan) dan menyimpan file pointer-nya di `log_file`.
    - Menggunakan `fuse_main` untuk menjalankan sistem berkas FUSE.

Selanjutnya, compile kode modular.c
```
gcc -Wall 'pkg-config fuse --cflags' modular.c -o fuse 'pkg-config fuse --libs'
```
Berikut merupakan hasil dari ketentuan 4a-4e
a.	Pada filesystem tersebut, jika Bagong membuat atau me-rename sebuah direktori dengan awalan module_, maka direktori tersebut akan menjadi direktori modular. 

![4a.png](Dokumentasi/Soal 4/4a.png)

![4b.png](Dokumentasi/Soal 4/4b.png)

b.	Bagong menginginkan agar saat melakukan modularisasi pada suatu direktori, maka modularisasi tersebut juga berlaku untuk konten direktori lain di dalam direktori (subdirektori) tersebut.

![4b.png](Dokumentasi/Soal 4/4b.png)

c.	Sebuah file nantinya akan terbentuk bernama fs_module.log pada direktori home pengguna (/home/[user]/fs_module.log) yang berguna menyimpan daftar perintah system call yang telah dijalankan dengan sesuai dengan ketentuan yang telah disebutkan sebelumnya.

![4c.png](Dokumentasi/Soal 4/4c.png)

d.	Saat Bagong melakukan modularisasi, file yang sebelumnya ada pada direktori asli akan menjadi file-file kecil sebesar 1024 bytes dan menjadi normal ketika diakses melalui filesystem yang dirancang oleh dia sendiri.
Contoh:
file File_Bagong.txt berukuran 3 kB pada direktori asli akan menjadi 3 file kecil, yakni File_Bagong.txt.000, File_Bagong.txt.001, dan File_Bagong.txt.002 yang masing-masing berukuran 1 kB (1 kiloBytes atau 1024 bytes).

![4d.png](Dokumentasi/Soal 4/4d.png)

e. Apabila sebuah direktori modular di-rename menjadi tidak modular, maka isi atau konten direktori tersebut akan menjadi utuh kembali (fixed).

![4e.png](Dokumentasi/Soal 4/4e.png)

---
### Soal5
a. Program rahasia.c merupakan file FUSE yang akan digunakan untuk melakukan mount folder pada Docker Container. Unduh file rahasia.zip kemudian lakukan unzip pada file rahasia.zip menjadi folder rahasia.

Jawab : 
Pertama-tama kita download terlebih dahulu filenya dengan menggunakan fungsi linux yaitu wget. Kodenya adalah sebagai berikut:
```
    system("wget --load-cookies /tmp/cookies.txt \"https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=FILEID' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\\1\\n/p')&id=18YCFdG658SALaboVJUHQIqeamcfNY39a\" -O /home/gmaaliki/rahasia.zip && rm -rf /tmp/cookies.txt");
    system("unzip /home/gmaaliki/rahasia.zip -d /home/gmaaliki");

```
Disini saya menggunakan wget untuk mendownload filenya ke directory /home/gmaaliki/rahasia.zip dan unzip untuk melakukan unzip ke directory /home/gmaaliki/rahasia
  
Gambar user mendaftar 2 akun yang sama
![no5.1](Dokumentasi/Soal5/3.1.png)
  
Gambar user berhasil login
![no5.2](Dokumentasi/Soal5/3.2.png)
b. Seperti soal a, folder rahasia akan di-mount pada Docker Container dengan image bernama rahasia_di_docker_<Kode Kelompok> pada direktori /usr/share. Gunakan Ubuntu Focal Fossa sebagai base image pada Dockerfile.

Jawab :
Pertama-tama saya membuat file docker-compose.yaml untuk melakukan integrasi fuse ke docker
```
version: "3"
services:
   myapp:
       image: rahasia_di_docker_d08
       container_name: rahasia_docker
       command: tail -f /dev/null
       volumes:
       - /home/gmaaliki/rahasia:/usr/share

```
Untuk membuat imagenya saya membuat sebuah container dengan base image Ubuntu Focal Fossa lalu saya stop dan saya commit sehingga saya bisa menggunakan imagenya
```
    system("docker pull ubuntu:focal");
    system("docker run -d --name tmp_container ubuntu:focal");
    system("docker stop tmp_container");

    system("docker commit tmp_container rahasia_di_docker_d08");
    system("docker rm tmp_container");
    system("docker-compose up -d");
```
Dapat dilihat di atas saya melakukan pull lalu saya melakukan run dengan base image Ubuntu Focal Fossa. Lalu saya stop dan mengcommit containernya sehingga saya mendapatkan image yg bernama rahasia_di_docker_d08. Lalu saya melakukan docker-compose dengan file docker-compose.yml yang tertera sebelumnya.
   
Gambar image rahasia_di_docker_d08 berhasil dibuat dan container rahasia_docker dijalankan (sebelah kiri)
![no5.3](Dokumentasi/Soal5/3.3.png)  
  
Gambar shell didalam container rahasia_docker dan directory rahasia berhasil dimount dengan menggunakan docker volume  
![no5.8](Dokumentasi/Soal5/3.8.png)  
  
c. Setelah melakukan mount, buatlah register system yang menyimpan kredensial berupa username dan password. Agar lebih aman, password disimpan dengan menggunakan hashing MD5. Untuk mempermudah kalian, gunakan system()  serta gunakan built-in program untuk melakukan hashing MD5 pada Linux (tidak wajib). Username dan password akan disimpan dengan format <username>;<password>. Tidak boleh ada user yang melakukan register dengan username yang sama. Kemudian, Buatlah login system yang mencocokkan kredensial antara username dan password.
Contoh:
kyunkyun;fbc5ccdf7c34390d07b1f4b74958a9ce
Penjelasan:
kyunkyun adalah username.
fbc5ccdf7c34390d07b1f4b74958a9ce adalah MD5 Hashing dari Subarukun, user kyunkyun menginputkan Subarukun sebagai password. Untuk lebih jelas, kalian bisa membaca dokumentasinya pada https://www.md5hashgenerator.com/. 
Catatan: 
Tidak perlu ada password validation untuk mempermudah kalian

Jawab :
Saya menerapkan fungsi login dan register tersebut di awal eksekusi kode. Nantinya apabila tidak diterima loginnya maka kode tidak akan berlajut dan akses ditolak. Saya meminta username dan password dari user yang langsung saya ubah dengan md5 menggunakan perintah di linux yaitu md5sum, dan tergantung dari pilihan user di awal, perlakuannya akan berbeda. Apabila user memilih untuk login maka program akan memeriksa semua user yang terdaftar pada file `registered_user.txt` dan mencocokan nama serta passwordnya dan apabila cocok maka akses akan diberikan. Lalu apabila tidak cocok akses akan ditolak. Sedangkan apabila user memilih untuk regiser maka program pertama akan mencari apakah user sudah ada, apabila belum maka akan terdaftar dan bila sudah program akan memberikan error dan memprompt user untuk mencoba lagi
```
int authenticate_user() {
    system("touch registered_user.txt");
    printf("Press 1 to login\n");
    printf("Press 2 to register\n");
    printf("Press any other key to quit\n");
    printf(" -> ");

    int num;
    scanf("%d", &num);

    char name[32], pass[32];
    printf("Enter name: ");
    scanf("%s", name);

    printf("Enter pass: ");
    scanf("%s", pass);

    FILE *fptr;

    // generate hash for the pass
    char hash_command[128];
    sprintf(hash_command, "echo %s > temp1.txt; md5sum temp1.txt | awk '{print $1}' > temp2.txt", pass);
    system(hash_command);

    fptr = fopen("temp2.txt", "r");
    
    char hash_pass[128];
    fscanf(fptr, "%s", hash_pass);

    fclose(fptr);
    system("rm temp1.txt");
    system("rm temp2.txt");
    
    if(num == 1) { // check for the same user and authenticate
        char input_file[300];
        char delimiter[]=";";
        fptr = fopen("registered_user.txt", "r");

        while(fgets(input_file,sizeof(input_file), fptr) != NULL) {
            //get the name from [name];[hash]
            char *token = strtok(input_file, delimiter);
            if(strcmp(token, name) == 0) {
                // get the hash from [name];[hash]
                token = strtok(NULL, delimiter);
                // remove newline
                token[strcspn(token, "\n")] = 0;
                if(strcmp(token, hash_pass) == 0) {
                    isAllowed = true;
                    break;
                }
            }
        }
    } else if(num == 2) { // write the previous input to a file
        char input_file[300];
        char delimiter[]=";";
        fptr = fopen("registered_user.txt", "r");

        while(fgets(input_file, sizeof(input_file), fptr) != NULL) {
            char *token = strtok(input_file, delimiter);
            if(strcmp(token, name) == 0) {
                printf("Username already used!");
                authenticate_user();
                return 0;
            }
        }

        char input_user[300];
        sprintf(input_user, "%s;%s", name, hash_pass);

        char input_command[512];
        sprintf(input_command, "echo '%s' >> registered_user.txt", input_user);
        system(input_command);

        authenticate_user();
    } else {
        return -1;
    }
    return 0;
}

```
Gambar isi `registered_user`  
![no5.10](Dokumentasi/Soal5/3.10.png)

d. Folder tersebut hanya dapat diakses oleh user yang telah melakukan login. User yang login dapat membaca folder dan file yang di-mount kemudian dengan menggunakan FUSE lakukan rename pada folder menjadi <Nama_Folder>_<Kode_Kelompok> dan <Kode_Kelompok>_<Nama_File>.<ext>.
Contoh:
A01_a.pdf
xP4UcxRZE5_A01

Jawab :
Untuk melakukan rename saya menggunakan xmp_rename yang mengambil 2 argumen `oldpath` dan `newpath`. `oldpath` diisi dengan yang ingin diubah sedangkan `newpath` diisi dengan perubahan yang diinginkan. Saya lalu memanggil `xmp_rename` di fungsi `xmp_readdir` untuk merename directory dan `xmp_open` untuk merename file. Keduanya akan terename apabila dibuka. Contohnya membuka file dengan `cat`. Untuk directory agak berbeda karena untuk mengubah nama kita harus menggunakan `ls` ke directorynya karena `readdir` dilaukan saat `ls`.
Berikut fungsi `xmp_rename`:
```
int xmp_rename(const char *oldpath, const char *newpath)
{
    char *filename = strrchr(oldpath, '/');
    if (filename != NULL) {
        filename++;
    }

    if(hasSubstring(filename, kelompok) || (strcmp(oldpath, dirpath)) == 0) {
        return 0;
    }

    int ret = rename(oldpath, newpath);
    printf("string awal: %s\nstring akhir: %s\n", oldpath, newpath);
    if (ret == -1) {
        perror("rename");
        return -errno;
    }

    return 0;
}
```

Berikut fungsi `xmp_readdir` :
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{

    if(!isAllowed) return -EACCES;

    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char name[2000];
    sprintf(name, "%s_%s", fpath, kelompok);
    xmp_rename(fpath, name);

    char modified_path[1000];
    if (strcmp(path, "/") != 0)
    {
        sprintf(modified_path, "%s_%s", path, kelompok);
        path = modified_path;
    }

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

```

Berikut fungsi `xmp_open` :
```
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    if (!isAllowed) return -EACCES;
    
    int res; char fpath[1000];
    
    sprintf(fpath, "%s%s", dirpath, path);
    
    res = open(fpath, fi->flags);
    if (res == -1) return -errno;
    
    fi->fh = res;
    return 0;
}

```
Gambar sebuah directory di `ls` dan namanya berubah (sebelah kanan)
![no5.3.2](Dokumentasi/Soal5/3.3.png)

Gambar sebuah file di `cat` dan namanya berubah
![no5.4](Dokumentasi/Soal5/3.4.png)
![no5.5](Dokumentasi/Soal5/3.5.png)

e. List seluruh folder, subfolder, dan file yang telah di-rename dalam file result.txt menggunakan tree kemudian hitung file tersebut berdasarkan extension dan output-kan menjadi extension.txt.

Jawab : 
Kita dapat menggunakan fungsi `tree` yang ada di linux untuk membentuk treenya. Dan untuk mencari tahu jumlah file dengan extension tertentu, kita bisa menggunakan kombinasi `cat` ke result.txt dan `grep` serta menggunakan `wc -l` untuk menghitung jumlahnya (`sed` hanya untuk formatting). Berikut cuplikan kodenya
```
    system("tree /home/gmaaliki/rahasia| grep d08 > result.txt");
    system("cat ./result.txt| grep -v \".txt\\|.png\\|.gif\\|.mp3\\|.jpg\\|.pdf\\|.docx\" | wc -l | sed 's/^/dir: /' > extension.txt");
    system("cat ./result.txt| grep .txt | wc -l | sed 's/^/txt: /'  >> extension.txt");
    system("cat ./result.txt| grep .png | wc -l | sed 's/^/png: /'  >> extension.txt");
    system("cat ./result.txt| grep .gif | wc -l | sed 's/^/gif: /'  >> extension.txt");
    system("cat ./result.txt| grep .mp3 | wc -l | sed 's/^/mp3: /'  >> extension.txt");
    system("cat ./result.txt| grep .jpg | wc -l | sed 's/^/jpg: /'  >> extension.txt");
    system("cat ./result.txt| grep .pdf | wc -l | sed 's/^/pdf: /'  >> extension.txt");
    system("cat ./result.txt| grep .docx | wc -l | sed 's/^/docx: /'  >> extension.txt");

``` 

Berikut adalah penjelasan umum dari semua kodenya : 
```
#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <fuse.h>
#include <sys/stat.h>

static const char *dirpath = "/home/gmaaliki/rahasia";
bool isAllowed = false;
char kelompok[] = "d08";

int authenticate_user() {
    system("touch registered_user.txt");
    printf("Press 1 to login\n");
    printf("Press 2 to register\n");
    printf("Press any other key to quit\n");
    printf(" -> ");

    int num;
    scanf("%d", &num);

    char name[32], pass[32];
    printf("Enter name: ");
    scanf("%s", name);

    printf("Enter pass: ");
    scanf("%s", pass);

    FILE *fptr;

    // generate hash for the pass
    char hash_command[128];
    sprintf(hash_command, "echo %s > temp1.txt; md5sum temp1.txt | awk '{print $1}' > temp2.txt", pass);
    system(hash_command);

    fptr = fopen("temp2.txt", "r");
    
    char hash_pass[128];
    fscanf(fptr, "%s", hash_pass);

    fclose(fptr);
    system("rm temp1.txt");
    system("rm temp2.txt");
    
    if(num == 1) { // check for the same user and authenticate
        char input_file[300];
        char delimiter[]=";";
        fptr = fopen("registered_user.txt", "r");

        while(fgets(input_file,sizeof(input_file), fptr) != NULL) {
            //get the name from [name];[hash]
            char *token = strtok(input_file, delimiter);
            if(strcmp(token, name) == 0) {
                // get the hash from [name];[hash]
                token = strtok(NULL, delimiter);
                // remove newline
                token[strcspn(token, "\n")] = 0;
                if(strcmp(token, hash_pass) == 0) {
                    isAllowed = true;
                    break;
                }
            }
        }
    } else if(num == 2) { // write the previous input to a file
        char input_file[300];
        char delimiter[]=";";
        fptr = fopen("registered_user.txt", "r");

        while(fgets(input_file, sizeof(input_file), fptr) != NULL) {
            char *token = strtok(input_file, delimiter);
            if(strcmp(token, name) == 0) {
                printf("Username already used!");
                authenticate_user();
                return 0;
            }
        }

        char input_user[300];
        sprintf(input_user, "%s;%s", name, hash_pass);

        char input_command[512];
        sprintf(input_command, "echo '%s' >> registered_user.txt", input_user);
        system(input_command);

        authenticate_user();
    } else {
        return -1;
    }
    return 0;
}

int hasSubstring(const char *str, const char *substr) {
    int strLen = strlen(str);
    int substrLen = strlen(substr);

    for (int i = 0; i <= strLen - substrLen; i++) {
        int j;

        for (j = 0; j < substrLen; j++) {
            if (str[i + j] != substr[j])
                break;
        }

        if (j == substrLen)
            return 1;
    }
    return 0;
}

int xmp_rename(const char *oldpath, const char *newpath)
{
    char *filename = strrchr(oldpath, '/');
    if (filename != NULL) {
        filename++;
    }

    if(hasSubstring(filename, kelompok) || (strcmp(oldpath, dirpath)) == 0) {
        return 0;
    }

    int ret = rename(oldpath, newpath);
    printf("string awal: %s\nstring akhir: %s\n", oldpath, newpath);
    if (ret == -1) {
        perror("rename");
        return -errno;
    }

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    if(!isAllowed) return -EACCES;

    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{

    if(!isAllowed) return -EACCES;

    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char name[2000];
    sprintf(name, "%s_%s", fpath, kelompok);
    xmp_rename(fpath, name);

    char modified_path[1000];
    if (strcmp(path, "/") != 0)
    {
        sprintf(modified_path, "%s_%s", path, kelompok);
        path = modified_path;
    }

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if(!isAllowed) return -EACCES;

    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res;
    FILE *file;

    file = fdopen(fi->fh, "r");
    if (file == NULL)
        return -errno;

    res = fseek(file, offset, SEEK_SET);
    if (res == -1) {
        fclose(file);
        return -errno;
    }

    res = fread(buf, sizeof(char), size, file);
    if (res == -1) {
        fclose(file);
        return -errno;
    }

    char modifiedPath[100];

    // memformat nama sesuai ketentuan soal e.g: d08_file.txt
    char *filename = strrchr(fpath, '/');
    if (filename != NULL) {
        filename++;
    }

    size_t filenameLength = strlen(filename);
    strncpy(modifiedPath, fpath, strlen(fpath) - filenameLength);
    modifiedPath[strlen(fpath) - filenameLength] = '\0';

    char appended[5];
    sprintf(appended, "%s_", kelompok);
    strcat(modifiedPath, appended);
    strcat(modifiedPath, filename);

    xmp_rename(fpath, modifiedPath);

    fclose(file);

    system("tree /home/gmaaliki/rahasia| grep d08 > result.txt");
    system("cat ./result.txt| grep -v \".txt\\|.png\\|.gif\\|.mp3\\|.jpg\\|.pdf\\|.docx\" | wc -l | sed 's/^/dir: /' > extension.txt");
    system("cat ./result.txt| grep .txt | wc -l | sed 's/^/txt: /'  >> extension.txt");
    system("cat ./result.txt| grep .png | wc -l | sed 's/^/png: /'  >> extension.txt");
    system("cat ./result.txt| grep .gif | wc -l | sed 's/^/gif: /'  >> extension.txt");
    system("cat ./result.txt| grep .mp3 | wc -l | sed 's/^/mp3: /'  >> extension.txt");
    system("cat ./result.txt| grep .jpg | wc -l | sed 's/^/jpg: /'  >> extension.txt");
    system("cat ./result.txt| grep .pdf | wc -l | sed 's/^/pdf: /'  >> extension.txt");
    system("cat ./result.txt| grep .docx | wc -l | sed 's/^/docx: /'  >> extension.txt");

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    if (!isAllowed) return -EACCES;
    
    int res; char fpath[1000];
    
    sprintf(fpath, "%s%s", dirpath, path);
    
    res = open(fpath, fi->flags);
    if (res == -1) return -errno;
    
    fi->fh = res;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
    .read = xmp_read,
    .open = xmp_open,
};

int main(int argc, char *argv[]) {
    authenticate_user();
    if(isAllowed) {
        // Download dan unzip file dari google drive
        //system("wget --load-cookies /tmp/cookies.txt \"https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=FILEID' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\\1\\n/p')&id=18YCFdG658SALaboVJUHQIqeamcfNY39a\" -O /home/gmaaliki/rahasia.zip && rm -rf /tmp/cookies.txt");
        //system("unzip /home/gmaaliki/rahasia.zip -d /home/gmaaliki");

        // Membuat docker image
        umask(0);
        system("docker pull ubuntu:focal");
        system("docker run -d --name tmp_container ubuntu:focal");
        system("docker stop tmp_container");

        system("docker commit tmp_container rahasia_di_docker_d08");
        system("docker rm tmp_container");
        system("docker-compose up -d");

        system("mkdir /home/gmaaliki/fuse");
        system("sudo chmod 777 /home/gmaaliki/rahasia");
        system("sudo chmod 777 /home/gmaaliki/fuse");

        fuse_main(argc, argv, &xmp_oper, NULL);
    }

    return 0;
}

```
Gambar `cat` dari file result.txt dan extension.txt
![no5.7](Dokumentasi/Soal5/3.7.png)  
  
Penjelasan Umum :
Fungsi terdefinisi :
- authenticate_user(): Fungsi ini digunakan untuk melakukan otentikasi pengguna. Pengguna diminta untuk melakukan login atau registrasi dengan memasukkan nama dan kata sandi. Nama dan kata sandi kemudian di-hash menggunakan algoritma MD5. Jika pengguna memilih login, fungsi ini akan memeriksa apakah pengguna terdaftar dan kata sandi yang dimasukkan cocok. Jika pengguna memilih registrasi, fungsi ini akan menyimpan nama dan kata sandi pengguna yang baru ke dalam file "registered_user.txt".

- hasSubstring(const char *str, const char *substr): Fungsi ini digunakan untuk memeriksa apakah sebuah substring ada dalam sebuah string. Mengembalikan nilai 1 jika substring ditemukan dan 0 jika tidak.

- xmp_rename(const char *oldpath, const char *newpath): Fungsi ini dipanggil saat ada pemindahan (rename) file. Fungsi ini memeriksa apakah string kelompok ada di dalam nama file atau apakah oldpath adalah direktori yang diperbolehkan. Jika tidak, fungsi ini akan memindahkan file menggunakan fungsi rename().

- xmp_getattr(const char *path, struct stat *stbuf): Fungsi ini digunakan untuk mendapatkan atribut (metadata) dari file atau direktori yang ditentukan oleh path. Fungsi ini mengembalikan informasi atribut menggunakan struct stat.

- xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi): Fungsi ini digunakan untuk membaca isi direktori yang ditentukan oleh path. Fungsi ini membuka direktori, membaca setiap entri di dalamnya, dan mengisi buffer dengan nama-nama entri menggunakan fungsi filler. Fungsi ini juga melakukan pemindahan (rename) pada direktori sebelum mengembalikan entri-entri tersebut.

- xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi): Fungsi ini digunakan untuk membaca isi file yang ditentukan oleh path. Fungsi ini membuka file, mengatur offset, dan membaca sejumlah byte yang diminta ke dalam buffer buf. Fungsi ini juga melakukan pemindahan (rename) pada file sebelum mengembalikan hasil pembacaan.

- xmp_open(const char *path, struct fuse_file_info *fi): Fungsi ini dipanggil saat ada permintaan pembukaan (open) file. Fungsi ini membuka file yang ditentukan oleh path dengan menggunakan flag yang terdapat pada fi->flags. Jika pembukaan berhasil, fungsi ini mengatur file handle (fi->fh) dan mengembalikan 0.

Fungsi tidak terdefinisi : 
- lstat(const char *path, struct stat *stbuf): Fungsi ini digunakan untuk mendapatkan informasi atribut (metadata) dari file atau direktori yang ditentukan oleh path. Perbedaannya dengan stat() adalah bahwa lstat() tidak mengikuti tautan simbolik dan memberikan informasi tentang tautan itu sendiri, bukan file/direktori yang ditautkan.

- strrchr(const char *str, int c): Fungsi ini digunakan untuk mencari kemunculan terakhir karakter c dalam string str. Fungsi ini mengembalikan pointer ke karakter tersebut atau NULL jika tidak ditemukan.

- fopen(const char *path, const char *mode): Fungsi ini digunakan untuk membuka file yang ditentukan oleh path dengan mode yang ditentukan oleh mode. Mode dapat berupa "r" (baca), "w" (tulis), "a" (tambah), dan variasi lainnya. Fungsi ini mengembalikan pointer ke struktur FILE yang mewakili file yang dibuka.

- fclose(FILE *stream): Fungsi ini digunakan untuk menutup file yang telah dibuka yang diidentifikasikan oleh pointer stream.

- fgets(char *str, int n, FILE *stream): Fungsi ini digunakan untuk membaca baris dari file yang diidentifikasikan oleh stream dan menyimpannya dalam string str. Maksimum n-1 karakter dibaca.

- system(const char *command): Fungsi ini digunakan untuk menjalankan perintah shell yang ditentukan oleh command. Fungsi ini akan mengeksekusi perintah dan mengembalikan hasil eksekusi.

- strcmp(const char *str1, const char *str2): Fungsi ini digunakan untuk membandingkan dua string str1 dan str2. Fungsi ini mengembalikan 0 jika kedua string identik, nilai negatif jika str1 lebih kecil dari str2, dan nilai positif jika str1 lebih besar dari str2.

- strtok(char *str, const char *delim): Fungsi ini digunakan untuk memecah string str menjadi token-token tergantung pada karakter pemisah yang ditentukan oleh delim. Fungsi ini mengembalikan pointer ke token berikutnya atau NULL jika tidak ada token lagi.

- sprintf(char *str, const char *format, ...): Fungsi ini digunakan untuk memformat string dengan menggunakan format yang ditentukan oleh format dan menghasilkan output yang disimpan di dalam str.

- md5sum: Ini adalah perintah shell yang digunakan untuk menghitung hash MD5 dari suatu teks atau file.

- rename(const char *oldpath, const char *newpath): Fungsi ini digunakan untuk mengubah nama atau memindahkan file dari oldpath ke newpath. Jika newpath sudah ada, maka rename() akan menggantinya.

- perror(const char *s): Fungsi ini digunakan untuk mencetak pesan kesalahan terkait

Kendala :
- Pada demo poin e saya masih salah di bagian extension.txt. Tetapi saya sudah dapat perbaikannya dengan mengganti tree dengan cat sehingga saya bisa mengambil isi dari ./result.txt dan menggunakan `grep` dan `wc -l` untuk menghitung jumlahnya

