#define FUSE_USE_VERSION 28
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <dirent.h>
#include <errno.h>
#include <unistd.h>
#include <fuse.h>
#include <sys/stat.h>

static const char *dirpath = "/home/gmaaliki/rahasia";
bool isAllowed = false;
char kelompok[] = "d08";

int authenticate_user() {
    system("touch registered_user.txt");
    printf("Press 1 to login\n");
    printf("Press 2 to register\n");
    printf("Press any other key to quit\n");
    printf(" -> ");

    int num;
    scanf("%d", &num);

    char name[32], pass[32];
    printf("Enter name: ");
    scanf("%s", name);

    printf("Enter pass: ");
    scanf("%s", pass);

    FILE *fptr;

    // generate hash for the pass
    char hash_command[128];
    sprintf(hash_command, "echo %s > temp1.txt; md5sum temp1.txt | awk '{print $1}' > temp2.txt", pass);
    system(hash_command);

    fptr = fopen("temp2.txt", "r");
    
    char hash_pass[128];
    fscanf(fptr, "%s", hash_pass);

    fclose(fptr);
    system("rm temp1.txt");
    system("rm temp2.txt");
    
    if(num == 1) { // check for the same user and authenticate
        char input_file[300];
        char delimiter[]=";";
        fptr = fopen("registered_user.txt", "r");

        while(fgets(input_file,sizeof(input_file), fptr) != NULL) {
            //get the name from [name];[hash]
            char *token = strtok(input_file, delimiter);
            if(strcmp(token, name) == 0) {
                // get the hash from [name];[hash]
                token = strtok(NULL, delimiter);
                // remove newline
                token[strcspn(token, "\n")] = 0;
                if(strcmp(token, hash_pass) == 0) {
                    isAllowed = true;
                    break;
                }
            }
        }
    } else if(num == 2) { // write the previous input to a file
        char input_file[300];
        char delimiter[]=";";
        fptr = fopen("registered_user.txt", "r");

        while(fgets(input_file, sizeof(input_file), fptr) != NULL) {
            char *token = strtok(input_file, delimiter);
            if(strcmp(token, name) == 0) {
                printf("Username already used!");
                authenticate_user();
                return 0;
            }
        }

        char input_user[300];
        sprintf(input_user, "%s;%s", name, hash_pass);

        char input_command[512];
        sprintf(input_command, "echo '%s' >> registered_user.txt", input_user);
        system(input_command);

        authenticate_user();
    } else {
        return -1;
    }
    return 0;
}

int hasSubstring(const char *str, const char *substr) {
    int strLen = strlen(str);
    int substrLen = strlen(substr);

    for (int i = 0; i <= strLen - substrLen; i++) {
        int j;

        for (j = 0; j < substrLen; j++) {
            if (str[i + j] != substr[j])
                break;
        }

        if (j == substrLen)
            return 1;
    }
    return 0;
}

int xmp_rename(const char *oldpath, const char *newpath)
{
    char *filename = strrchr(oldpath, '/');
    if (filename != NULL) {
        filename++;
    }

    if(hasSubstring(filename, kelompok) || (strcmp(oldpath, dirpath)) == 0) {
        return 0;
    }

    int ret = rename(oldpath, newpath);
    printf("string awal: %s\nstring akhir: %s\n", oldpath, newpath);
    if (ret == -1) {
        perror("rename");
        return -errno;
    }

    return 0;
}

static int xmp_getattr(const char *path, struct stat *stbuf)
{
    if(!isAllowed) return -EACCES;

    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{

    if(!isAllowed) return -EACCES;

    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    char name[2000];
    sprintf(name, "%s_%s", fpath, kelompok);
    xmp_rename(fpath, name);

    char modified_path[1000];
    if (strcmp(path, "/") != 0)
    {
        sprintf(modified_path, "%s_%s", path, kelompok);
        path = modified_path;
    }

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(fpath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        res = (filler(buf, de->d_name, &st, 0));

        if(res!=0) break;
    }

    closedir(dp);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    if(!isAllowed) return -EACCES;

    char fpath[1000];

    if(strcmp(path,"/") == 0)
    {
        path=dirpath;
        sprintf(fpath,"%s",path);
    } else sprintf(fpath, "%s%s",dirpath,path);

    int res;
    FILE *file;

    file = fdopen(fi->fh, "r");
    if (file == NULL)
        return -errno;

    res = fseek(file, offset, SEEK_SET);
    if (res == -1) {
        fclose(file);
        return -errno;
    }

    res = fread(buf, sizeof(char), size, file);
    if (res == -1) {
        fclose(file);
        return -errno;
    }

    char modifiedPath[100];

    // memformat nama sesuai ketentuan soal e.g: d08_file.txt
    char *filename = strrchr(fpath, '/');
    if (filename != NULL) {
        filename++;
    }

    size_t filenameLength = strlen(filename);
    strncpy(modifiedPath, fpath, strlen(fpath) - filenameLength);
    modifiedPath[strlen(fpath) - filenameLength] = '\0';

    char appended[5];
    sprintf(appended, "%s_", kelompok);
    strcat(modifiedPath, appended);
    strcat(modifiedPath, filename);

    xmp_rename(fpath, modifiedPath);

    fclose(file);

    system("tree /home/gmaaliki/rahasia| grep d08 > result.txt");
    system("cat ./result.txt| grep -v \".txt\\|.png\\|.gif\\|.mp3\\|.jpg\\|.pdf\\|.docx\" | wc -l | sed 's/^/dir: /' > extension.txt");
    system("cat ./result.txt| grep .txt | wc -l | sed 's/^/txt: /'  >> extension.txt");
    system("cat ./result.txt| grep .png | wc -l | sed 's/^/png: /'  >> extension.txt");
    system("cat ./result.txt| grep .gif | wc -l | sed 's/^/gif: /'  >> extension.txt");
    system("cat ./result.txt| grep .mp3 | wc -l | sed 's/^/mp3: /'  >> extension.txt");
    system("cat ./result.txt| grep .jpg | wc -l | sed 's/^/jpg: /'  >> extension.txt");
    system("cat ./result.txt| grep .pdf | wc -l | sed 's/^/pdf: /'  >> extension.txt");
    system("cat ./result.txt| grep .docx | wc -l | sed 's/^/docx: /'  >> extension.txt");

    return res;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    if (!isAllowed) return -EACCES;
    
    int res; char fpath[1000];
    
    sprintf(fpath, "%s%s", dirpath, path);
    
    res = open(fpath, fi->flags);
    if (res == -1) return -errno;
    
    fi->fh = res;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .rename = xmp_rename,
    .read = xmp_read,
    .open = xmp_open,
};

int main(int argc, char *argv[]) {
    authenticate_user();
    if(isAllowed) {
        // Download dan unzip file dari google drive
        //system("wget --load-cookies /tmp/cookies.txt \"https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=FILEID' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\\1\\n/p')&id=18YCFdG658SALaboVJUHQIqeamcfNY39a\" -O /home/gmaaliki/rahasia.zip && rm -rf /tmp/cookies.txt");
        //system("unzip /home/gmaaliki/rahasia.zip -d /home/gmaaliki");

        // Membuat docker image
        umask(0);
        system("docker pull ubuntu:focal");
        system("docker run -d --name tmp_container ubuntu:focal");
        system("docker stop tmp_container");

        system("docker commit tmp_container rahasia_di_docker_d08");
        system("docker rm tmp_container");
        system("docker-compose up -d");

        system("mkdir /home/gmaaliki/fuse");
        system("sudo chmod 777 /home/gmaaliki/rahasia");
        system("sudo chmod 777 /home/gmaaliki/fuse");

        fuse_main(argc, argv, &xmp_oper, NULL);
    }

    return 0;
}

